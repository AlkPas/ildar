class ParsingDevisesController < ApplicationController
  require "open-uri"
  require 'nokogiri'
  def create
    allSite = Hash.new
    eachSite = Hash.new
    # Пустой титульник для начала
    session[:site_id] = "Выберите сайт для поиска"
    # Собираем данные для 1го сайт
    # Адрес сайта
    eachSite[:site] = "http://www.gsmarena.com"
    # Строка поиска где %%search значение для поиска
    eachSite[:search] = "http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=%%search"
    # Элементы для парсинга возвращаемого списка поиска
    eachSite[:parsingList] = ".section-body .makers ul li a"

    # Наименование
    eachSite[:parsingItemName] = ".specs-phone-name-title"
    # Характеристики
    eachSite[:parsingItemBody] = "#specs-list table"
    allSite['gsmarena'] = eachSite
    session[:sites] = allSite
  end
  def index
    create
    @post = nil
  end
  def search
    @error = nil
    @phones = Hash.new
    site = session[:sites][session[:site_id]]
    if site.nil?
      @error = "Не выбран серевер для поиска"
    else
      if params[:post][:search].length == 0
        @error = "Нет условий поиска"
      else
        url = site['search'].sub('%%search',params[:post][:search].strip.gsub(" ", "+"))
        openUrl = open(url)
        openDocument = Nokogiri::HTML(openUrl)

        # Проверяем, что открыт именно список файлов а не сам файл
        if openDocument.css(site['parsingItemName']).count() > 0
          showPhone(openDocument)
          render "showPhone"
        else
          openDocument.css(site['parsingList']).each do |element|
            phone = Hash.new
            phone[:url] = site['site'] + '/' + element['href']
            element.css('img').each {|link| phone[:photo] = link['src'] }
            @phones[element.text] = phone
          end
        end
      end
    end
  end
  def showPhone(openDocument = nil)
    @specs = Array.new
    site = session[:sites][session[:site_id]]
    if openDocument.nil?
      openUrl = open(params[:showPhone][:url])
      openDocument = Nokogiri::HTML(openUrl)
    end
    openDocument.css(site['parsingItemName']).each do |element|
      @name = element.text
    end
    openDocument.css(site['parsingItemBody']).each do |element|
      element.css('tr').each do |tr|
        el = Array.new
        tr.css('th').each {|i| el[0] = i.text}
        if tr.css('th').count() == 0
          el << ""
        end
        tr.css('td').each {|i| el << i.text}
        @specs.push(el)
      end
    end

  end
end
