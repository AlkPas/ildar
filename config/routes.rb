Rails.application.routes.draw do

  # resources :parsing_devises
  get 'change/:id' => 'sessions#change', as: "change"
  post 'search' => 'parsing_devises#search', as: "search"
  post 'showPhone' => 'parsing_devises#showPhone', as: "showPhone"

  root 'parsing_devises#index'

end
